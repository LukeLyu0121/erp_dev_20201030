package tw.com.tm.erp.action;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;

import org.apache.commons.beanutils.PropertyUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.util.StringUtils;

import tw.com.tm.erp.constants.OrderStatus;
import tw.com.tm.erp.exceptions.NoSuchObjectException;
import tw.com.tm.erp.exceptions.ValidationErrorException;
import tw.com.tm.erp.hbm.bean.Command;
import tw.com.tm.erp.hbm.bean.DbcInformationTablesTXF;
import tw.com.tm.erp.hbm.bean.MessageBox;
import tw.com.tm.erp.hbm.service.DbcInformationTXFService;
import tw.com.tm.erp.utils.AjaxUtils;
import tw.com.tm.erp.utils.NumberUtils;

public class DbcInformationTXFAction {

	private static final Log log = LogFactory.getLog(DbcInformationTXFAction.class);
	private DbcInformationTXFService dbcInformationTXFService;

	public void setDbcInformationTXFService(DbcInformationTXFService dbcInformationTXFService) {
		this.dbcInformationTXFService = dbcInformationTXFService;
	}

	public List<Properties> performInitial(Map parameterMap) {
		Map returnMap = null;
		try {
			log.info("表格欄位資訊初始化");
			returnMap = dbcInformationTXFService.executeInitial(parameterMap);
		} catch (Exception ex) {
			log.error("執行表格欄位資訊存檔初始化時發生錯誤，原因：" + ex.toString());
			ex.printStackTrace();
			MessageBox msgBox = new MessageBox();
			msgBox.setMessage(ex.getMessage());
			returnMap = new HashMap();
			returnMap.put("vatMessage", msgBox);
		}
		return AjaxUtils.parseReturnDataToJSON(returnMap);
	}

	public List<Properties> performTransaction(Map parameterMap) {
		log.info("表格欄位資訊送出/暫存");
		Map returnMap = new HashMap();
		MessageBox msgBox = new MessageBox();
		try {
			Object formBindBean = parameterMap.get("vatBeanFormBind");
			Object otherBean = parameterMap.get("vatBeanOther");
			Long sysSno = NumberUtils.getLong((String) PropertyUtils.getProperty(formBindBean, "sysSno"));
			String formStatus = (String)PropertyUtils.getProperty(otherBean, "formStatus");
			String loginEmployeeCode = (String)PropertyUtils.getProperty(otherBean, "loginEmployeeCode");
			//判別前台formStatus是否有值
			if(StringUtils.hasText(formStatus)){
				if(OrderStatus.SAVE.equals(formStatus)){
					//取正式單號
					dbcInformationTXFService.saveActualOrderNo(sysSno, loginEmployeeCode);
					//更新異動檔及明細檔 (AjaxUtils.copyJSONBeantoPojoBean)
					returnMap = dbcInformationTXFService.updateTablesTXFBean(parameterMap);
					/** 後續簽核流程使用 */
					//更新至主檔,並修改狀態為Finish
					dbcInformationTXFService.saveFinishDbcInformation(sysSno, loginEmployeeCode);
	    		}
    		}else {
    			throw new ValidationErrorException("單據狀態參數為空值，無法執行存檔！");
    		}
			log.info("msgBox前");
			wrappedMessageBox(msgBox, true);
			msgBox.setMessage((String) returnMap.get("resultMsg"));
			log.info("msgBox後");
		} catch (Exception ex) {
			log.error("執行表格欄位修改修改失敗，原因：" + ex.toString());
			ex.printStackTrace();
		}
		returnMap.put("vatMessage", msgBox);
		return AjaxUtils.parseReturnDataToJSON(returnMap);
	}
	
	/**
	 * 綑綁MESSAGE 
	 * @param msgBox
	 * @param locationBean
	 * @param isStartProcess
	 */
	private void wrappedMessageBox(MessageBox msgBox, boolean isStartProcess){

		Command cmd_ok = new Command();
		if(isStartProcess){
			msgBox.setType(MessageBox.CONFIRM);
			cmd_ok.setCmd(Command.FUNCTION);
			cmd_ok.setParameters(new String[]{"createRefreshForm()", "","","",""});
			Command cmd_cancel = new Command();
			cmd_cancel.setCmd(Command.WIN_CLOSE);
			msgBox.setCancel(cmd_cancel);
		}

		msgBox.setOk(cmd_ok);
	}

}
