package tw.com.tm.erp.hbm.service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;

import org.apache.commons.beanutils.PropertyUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.util.StringUtils;

import tw.com.tm.erp.hbm.bean.DbcInformationTables;
import tw.com.tm.erp.hbm.dao.BaseDAO;
import tw.com.tm.erp.hbm.dao.DbcInformationTablesDAO;
import tw.com.tm.erp.utils.AjaxUtils;

public class DbcInformationService {
	private static final Log log = LogFactory.getLog(DbcInformationService.class);
	
	private DbcInformationTablesDAO dbcInformationTablesDAO;
	
	public void setDbcInformationTablesDAO(DbcInformationTablesDAO dbcInformationTablesDAO) {
		this.dbcInformationTablesDAO = dbcInformationTablesDAO;
	}
	
	
	 public static final String[] GRID_SEARCH_FIELD_NAMES = { 
			 	"area", "tableName", "tableComments", "enable"};
	 public static final String[] GRID_SEARCH_FIELD_DEFAULT_VALUES = { 
				"", "", "", ""};
	 public static final int[] GRID_SEARCH_FIELD_TYPES = { 
			 	AjaxUtils.FIELD_TYPE_STRING, AjaxUtils.FIELD_TYPE_STRING, AjaxUtils.FIELD_TYPE_STRING, AjaxUtils.FIELD_TYPE_STRING};

//	/**
//	 * eChange取area	帶下拉
//	 * 
//	 * @param httpRequest
//	 * @return
//	 * @throws Exception
//	 */
//	public List<Properties> getTableNameAJAX(Properties httpRequest) throws Exception{
//    	log.info("getTableNameAJAX");
//    	List<Properties> result = new ArrayList();
//    	Properties properties   = new Properties();
//    	
//		try{
//			String area = httpRequest.getProperty("area");
//
//			List <DbcInformationTables> dbcInformationSchemaList = dbcInformationTablesDAO.findByArea(area);
//			dbcInformationSchemaList = AjaxUtils.produceSelectorData(dbcInformationSchemaList, "tableName", "tableComments", true, true);
//			properties.setProperty("allTableName", AjaxUtils.parseSelectorData(dbcInformationSchemaList));
//			
//			result.add(properties);
//			return result;
//		}catch (Exception ex) {
//			log.info(ex.getMessage());
//			throw new Exception(ex.getMessage());
//		}
//    }
	
	/**
	 * ajax 查詢明細資料檔載入時,取得明細分頁資料
	 * 
	 * @param httpRequest
	 * @return
	 * @throws Exception
	 */
	public List<Properties> getAJAXSearchPageData(Properties httpRequest)throws Exception {
		try {
			List<Properties> result = new ArrayList();
			List<Properties> gridDatas = new ArrayList();
			
			int iSPage = AjaxUtils.getStartPage(httpRequest);// 取得起始頁面
			int iPSize = AjaxUtils.getPageSize(httpRequest);// 取得每頁大小
			
			/**帶入查詢的值*/
			String area = httpRequest.getProperty("area");// 品牌代號
			String tableName = httpRequest.getProperty("tableName");
			String enable = httpRequest.getProperty("enable");
			String schema = httpRequest.getProperty("schema");
			
			/**執行查詢*/
			HashMap map = new HashMap();
			HashMap findObjs = new HashMap();
			findObjs.put(" and model.area = :area", area);
			if(StringUtils.hasText(schema)) {
				findObjs.put(" and model.tableName like :tableName", schema + "%");
			}
			findObjs.put(" and model.tableName = :tableName", tableName);
			findObjs.put(" and model.enable = :enable", enable);

			List<DbcInformationTables> dbcInformationTablesList = (List<DbcInformationTables>) dbcInformationTablesDAO.search(
					"DbcInformationTables as model", findObjs, "order by tableName asc", iSPage, iPSize, BaseDAO.QUERY_SELECT_RANGE).get(BaseDAO.TABLE_LIST);
			System.out.println("dbcInformationTablesList.Size=["+ dbcInformationTablesList.size() + "]");
			 
			if (dbcInformationTablesList != null && dbcInformationTablesList.size() > 0) {
				// 取得第一筆的 INDEX
				Long firstIndex = Long.valueOf(iSPage * iPSize) + 1; 
				// 取得最後一筆 INDEX
				Long maxIndex = (Long) dbcInformationTablesDAO.search("DbcInformationTables as model",
						"count(model.id.tableName) as rowCount", findObjs,"order by tableName asc", BaseDAO.QUERY_RECORD_COUNT).get(BaseDAO.TABLE_RECORD_COUNT);
				log.info("maxIndex: "+maxIndex);
				
				result.add(AjaxUtils.getAJAXPageData(httpRequest, GRID_SEARCH_FIELD_NAMES, GRID_SEARCH_FIELD_DEFAULT_VALUES, dbcInformationTablesList, gridDatas, firstIndex, maxIndex));
			} else {
				result.add(AjaxUtils.getAJAXPageDataDefault(httpRequest, GRID_SEARCH_FIELD_NAMES, GRID_SEARCH_FIELD_DEFAULT_VALUES, map, gridDatas));
			}
			return result;
		} catch (Exception ex) {
			log.error("載入頁面顯示的表格資訊查詢發生錯誤，原因：" + ex.toString());
			ex.printStackTrace();
			throw new Exception("載入頁面顯示的表格資訊查詢失敗！");
		}
	}
	
	
	
	
	
	
}
