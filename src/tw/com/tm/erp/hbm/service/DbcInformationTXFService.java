package tw.com.tm.erp.hbm.service;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import org.apache.commons.beanutils.PropertyUtils;
import org.apache.commons.httpclient.util.DateUtil;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.util.StringUtils;
import tw.com.tm.erp.constants.MessageStatus;
import tw.com.tm.erp.constants.OrderStatus;
import tw.com.tm.erp.exceptions.FormException;
import tw.com.tm.erp.exceptions.NoSuchObjectException;
import tw.com.tm.erp.exceptions.ObtainSerialNoFailedException;
import tw.com.tm.erp.exceptions.ValidationErrorException;
import tw.com.tm.erp.hbm.bean.DbcInformationColumns;
import tw.com.tm.erp.hbm.bean.DbcInformationColumnsTXF;
import tw.com.tm.erp.hbm.bean.DbcInformationTables;
import tw.com.tm.erp.hbm.bean.DbcInformationTablesTXF;
import tw.com.tm.erp.hbm.dao.BaseDAO;
import tw.com.tm.erp.hbm.dao.DbcInformationColumnsDAO;
import tw.com.tm.erp.hbm.dao.DbcInformationColumnsTXFDAO;
import tw.com.tm.erp.hbm.dao.DbcInformationTablesDAO;
import tw.com.tm.erp.hbm.dao.DbcInformationTablesTXFDAO;
import tw.com.tm.erp.utils.AjaxUtils;
import tw.com.tm.erp.utils.DateUtils;
import tw.com.tm.erp.utils.NumberUtils;

public class DbcInformationTXFService {
	private static final Log log = LogFactory.getLog(DbcInformationTXFService.class);
	/**
	 * private fields
	 */
	private DbcInformationTablesTXFDAO dbcInformationTablesTXFDAO;
	private DbcInformationColumnsTXFDAO dbcInformationColumnsTXFDAO;
	private DbcInformationTablesDAO dbcInformationTablesDAO;
	private DbcInformationColumnsDAO dbcInformationColumnsDAO;
	private BuOrderTypeService buOrderTypeService;
	private DbcInformationService dbcInformationService;
	private BaseDAO baseDAO;
	/**
	 * set property accessors
	 */
	public void setDbcInformationTablesTXFDAO(DbcInformationTablesTXFDAO dbcInformationTablesTXFDAO) {
		this.dbcInformationTablesTXFDAO = dbcInformationTablesTXFDAO;
	}

	public void setDbcInformationColumnsTXFDAO(DbcInformationColumnsTXFDAO dbcInformationColumnsTXFDAO) {
		this.dbcInformationColumnsTXFDAO = dbcInformationColumnsTXFDAO;
	}
	
	public void setDbcInformationTablesDAO(DbcInformationTablesDAO dbcInformationTablesDAO) {
		this.dbcInformationTablesDAO = dbcInformationTablesDAO;
	}

	public void setDbcInformationColumnsDAO(DbcInformationColumnsDAO dbcInformationColumnsDAO) {
		this.dbcInformationColumnsDAO = dbcInformationColumnsDAO;
	}
	public void setBuOrderTypeService(BuOrderTypeService buOrderTypeService) {
		this.buOrderTypeService = buOrderTypeService;
	}
	public void setDbcInformationService(DbcInformationService dbcInformationService) {
		this.dbcInformationService = dbcInformationService;
	}
	public void setBaseDAO(BaseDAO baseDAO) {
		this.baseDAO = baseDAO;
	}

	/**
	 * 明細欄位
	 */
	public static final String[] GRID_FIELD_NAMES = 
		{"columnIndex","columnName", "columnComments","dataType",
		 "dataSize", "enable", "isRowId", "notNull" };

	public static final String[] GRID_FIELD_DEFAULT_VALUES = 
		{"", "", "", "", 
		 "", "", "", "" };

	public static final int[] GRID_FIELD_TYPES = 
		{ AjaxUtils.FIELD_TYPE_LONG, AjaxUtils.FIELD_TYPE_STRING, AjaxUtils.FIELD_TYPE_STRING, AjaxUtils.FIELD_TYPE_STRING,
		  AjaxUtils.FIELD_TYPE_LONG, AjaxUtils.FIELD_TYPE_STRING, AjaxUtils.FIELD_TYPE_STRING, AjaxUtils.FIELD_TYPE_STRING };
	
	/**
	 * 查詢明細欄位
	 */
	public static final String[] GRID_FIELD_NAMES_SEARCH = 
		{"columnName", "columnComments","dataType",
		 "dataSize", "enable", "isRowId", "notNull" };

	public static final String[] GRID_FIELD_DEFAULT_VALUES_SEARCH = 
		{"", "", "", 
		 "", "", "", "" };

	public static final int[] GRID_FIELD_TYPES_SEARCH = 
		{ AjaxUtils.FIELD_TYPE_STRING, AjaxUtils.FIELD_TYPE_STRING, AjaxUtils.FIELD_TYPE_STRING,
		  AjaxUtils.FIELD_TYPE_LONG, AjaxUtils.FIELD_TYPE_STRING, AjaxUtils.FIELD_TYPE_STRING, AjaxUtils.FIELD_TYPE_STRING };
	
	
	public Map executeInitial(Map parameterMap) throws Exception {
		log.info("executeInitial..");
		HashMap returnMap = new HashMap();

		try {
			Object otherBean = parameterMap.get("vatBeanOther");
			Long sysSno = NumberUtils.getLong((String) PropertyUtils.getProperty(otherBean, "sysSno"));
			String area = (String) PropertyUtils.getProperty(otherBean, "area");
			String tableName = (String) PropertyUtils.getProperty(otherBean, "tableName");
			
			DbcInformationTablesTXF dbcInformationTablesTXF = this.findActualDbcInformation(sysSno, otherBean, returnMap);
			if(StringUtils.hasText(area) && StringUtils.hasText(tableName)) {
				dbcInformationTablesTXF = this.executeFindDbcInformation(dbcInformationTablesTXF,otherBean);
				
			}
			returnMap.put("form", dbcInformationTablesTXF);

			return returnMap;
		} catch (Exception e) {
			log.error("取得實際主檔失敗,原因:" + e.toString());
			throw new Exception("取得實際主檔失敗,原因:" + e.toString());
		}
	}

	public DbcInformationTablesTXF findActualDbcInformation(Long sysSno, Object otherBean, Map returnMap)throws FormException, Exception {
		log.info("findDbcInformationTablesTXF..");
		DbcInformationTablesTXF form = null;
		form = sysSno == 0 ? executeNew(otherBean) : this.findById(sysSno);
		
		if (form != null) {
			returnMap.put("statusName", OrderStatus.getChineseWord(form.getStatus()));
			returnMap.put("sysModifierAmail", form.getSysModifierAmail());
			returnMap.put("sysLastUpdateTime", form.getSysLastUpdateTime());
			returnMap.put("form", form);
			return form;
		} else {
			throw new FormException("查無表格欄位資訊修改單主鍵：" + sysSno + "的資料！");
		}
	}
	
	
	public DbcInformationTablesTXF executeFindDbcInformation(DbcInformationTablesTXF dbcInformationTablesTXF, Object otherBean) throws Exception{
		log.info("executeFindDbcInformation..");
	    	try{
	    	    //======================取得複製時所需的必要資訊========================
	    		String loginEmployeeCode = (String) PropertyUtils.getProperty(otherBean, "loginEmployeeCode");
	    		String area = (String) PropertyUtils.getProperty(otherBean, "area");
				String tableName = (String) PropertyUtils.getProperty(otherBean, "tableName");
				
	    	    //======================get current return object==================
	    	    List<DbcInformationTables> dbcInformationTablesList = dbcInformationTablesDAO.findByProperty("DbcInformationTables",
	    	    		new String[]{"area","tableName", "enable"},
	    	    		new String[]{area,tableName, "Y"});
	    	    if(dbcInformationTablesList == null || dbcInformationTablesList.size()==0){
	    	    	log.info("查無表格資訊主檔的資料！");
	    	    }else{
			    //================copy Line Data==================
	    	    	DbcInformationTables dbcInformationTables = dbcInformationTablesList.get(0);
		    	    List<DbcInformationColumnsTXF> dbcInformationColumnsTXFList = new ArrayList();//新異動身
		    	    List<DbcInformationColumns> dbcInformationColumnsList = dbcInformationTables.getDbcInformationColumnsList();//主身
		    	    Long indexNo = 1L;
		    	    //主身回圈set值給異動身
		    	    for (DbcInformationColumns dbcInformationColumns : dbcInformationColumnsList) {
		    	    	DbcInformationColumnsTXF dbcInformationColumnsTXF = new DbcInformationColumnsTXF();
		    	    	dbcInformationColumnsTXF.setpSysSno(dbcInformationTablesTXF.getSysSno());
		    	    	dbcInformationColumnsTXF.setTableName(tableName);
		    	    	dbcInformationColumnsTXF.setColumnIndex(indexNo++);
		    	    	dbcInformationColumnsTXF.setColumnName(dbcInformationColumns.getColumnName());
		    	    	dbcInformationColumnsTXF.setColumnComments(dbcInformationColumns.getColumnComments());
		    	    	dbcInformationColumnsTXF.setDataType(dbcInformationColumns.getDataType());
		    	    	dbcInformationColumnsTXF.setDataSize(dbcInformationColumns.getDataSize());
		    	    	dbcInformationColumnsTXF.setEnable(dbcInformationColumns.getEnable());
		    	    	dbcInformationColumnsTXF.setIsRowId(dbcInformationColumns.getIsRowId());
		    	    	dbcInformationColumnsTXF.setNotNull(dbcInformationColumns.getNotNull());
		    	    	dbcInformationColumnsTXF.setSysModifierAmail(dbcInformationColumns.getSysModifierAmail());
		    	    	dbcInformationColumnsTXF.setSysLastUpdateTime(dbcInformationColumns.getSysLastUpdateTime());
		    	    	dbcInformationColumnsTXFList.add(dbcInformationColumnsTXF);
					}
		    	    //新的異動頭加入新的異動身
		    	    dbcInformationTablesTXF.setDbcInformationColumnsTXFList(dbcInformationColumnsTXFList);
		    	    dbcInformationTablesTXF.setArea(area);
		    	    dbcInformationTablesTXF.setTableName(tableName);
		    	    dbcInformationTablesTXF.setTableComments(dbcInformationTables.getTableComments());
		    	    dbcInformationTablesTXF.setSysLastUpdateTime(new Date());
		    	    dbcInformationTablesTXF.setSysModifierAmail(loginEmployeeCode);
		    	    dbcInformationTablesTXFDAO.update(dbcInformationTablesTXF);
	    	    }
	    		return dbcInformationTablesTXF;
	    	}catch(Exception ex){
	    		ex.printStackTrace();
	    	    log.error("查詢表格欄位資訊發生錯誤，原因：" + ex.toString());
	    	    throw new Exception("查詢表格欄位資訊發生錯誤，原因：" + ex.getMessage());
	    	}
		}

	/**
	 * find by PK
	 * 
	 * @param sysSno
	 * @return
	 */
	public DbcInformationTablesTXF findById(Long sysSno) {
		log.info("TablesTXF.findById..");
		return dbcInformationTablesTXFDAO.findById(sysSno);
	}
	/**
	 * find by unique
	 * 
	 * @param area,tableName
	 * @return
	 */
	public DbcInformationTablesTXF findByIdentification(String area, String tableName) {
		log.info("TablesTXF.findByIdentification..");
		return dbcInformationTablesTXFDAO.findByIdentification(area, tableName);
	}

	/**
	 * 產生新的表格欄位資訊修改單
	 * 
	 * @param otherBean
	 * @return
	 * @throws Exception
	 */
	public DbcInformationTablesTXF executeNew(Object otherBean) throws Exception {
		log.info("executeNew..");
		DbcInformationTablesTXF form = new DbcInformationTablesTXF();
		try {
			String loginEmployeeCode = (String) PropertyUtils.getProperty(otherBean, "loginEmployeeCode");
			String orderTypeCode = (String) PropertyUtils.getProperty(otherBean, "orderTypeCode");
			form.setEnable("Y");
			form.setOrderTypeCode(orderTypeCode);
			form.setStatus(OrderStatus.SAVE);
			form.setSysModifierAmail(loginEmployeeCode);
			form.setSysLastUpdateTime(new Date());
			this.saveHead(form);
			log.info("新建表格資料: "+form.getSysSno());
		} catch (Exception e) {
			log.error("建立表格欄位資訊修改主檔失敗,原因:" + e.toString());
			throw new Exception("建立新表格欄位資訊修改主檔失敗,原因:" + e.toString());
		}
		return form;
	}

	/**
	 * 預算修改存檔,取得暫存碼
	 * 
	 * @param imLetterOfCreditHead
	 * @throws Exception
	 */
	public void saveHead(DbcInformationTablesTXF dbcInformationTablesTXF) throws Exception {
		log.info("saveHead..");
		try {
			String tmpOrderNo = AjaxUtils.getTmpOrderNo();
			dbcInformationTablesTXF.setOrderNo(tmpOrderNo);
			dbcInformationTablesTXFDAO.save(dbcInformationTablesTXF);
		} catch (Exception ex) {
			log.error("取得暫時單號表格欄位資訊修改單發生錯誤，原因：" + ex.toString());
			throw new Exception("取得暫時單號表格欄位資訊修改單發生錯誤，原因：" + ex.getMessage());
		}
	}

	/**
	 * ajax 明細資料檔載入時,取得明細分頁資料
	 * 
	 * @param httpRequest
	 * @return
	 * @throws Exception
	 */
	public List<Properties> getAJAXLinePageData(Properties httpRequest) throws Exception {
		log.info("getAJAXLinePageData..");
		try {
			List<Properties> result = new ArrayList();
			List<Properties> gridDatas = new ArrayList();
			
			int iSPage = AjaxUtils.getStartPage(httpRequest);// 取得起始頁面
			int iPSize = AjaxUtils.getPageSize(httpRequest);// 取得每頁大小
			
			/**帶入查詢的值*/
			Long sysSno = NumberUtils.getLong(httpRequest.getProperty("sysSno"));// 要顯示的SYS_SNO
			log.info("sysSno=========="+sysSno);
			
			/**執行查詢*/
			HashMap findObjs = new HashMap();
			findObjs.put("and model.pSysSno = :pSysSno", sysSno);
			
			List<DbcInformationColumnsTXF> dbcInformationColumnsTXFList = (List<DbcInformationColumnsTXF>) dbcInformationColumnsTXFDAO.search(
					"DbcInformationColumnsTXF as model", findObjs, "order by model.id.columnIndex ", iSPage, iPSize, BaseDAO.QUERY_SELECT_RANGE).get(BaseDAO.TABLE_LIST);
			System.out.println("dbcInformationColumnsTXFList.Size=["+ dbcInformationColumnsTXFList.size() + "]");

			if (dbcInformationColumnsTXFList != null && dbcInformationColumnsTXFList.size() > 0) {

				// 取得第一筆的INDEX
				Long firstIndex = Long.valueOf(iSPage * iPSize) + 1; 
				// 取得最後一筆 INDEX
				Long maxIndex = (Long) baseDAO.search("DbcInformationColumnsTXF as model",
						"count(model.id.tableName) as rowCount", findObjs, BaseDAO.QUERY_RECORD_COUNT).get(BaseDAO.TABLE_RECORD_COUNT);
				log.info("maxIndex: "+maxIndex);
				
				result.add(AjaxUtils.getAJAXPageData(httpRequest, GRID_FIELD_NAMES_SEARCH, GRID_FIELD_DEFAULT_VALUES_SEARCH, dbcInformationColumnsTXFList, gridDatas, firstIndex, maxIndex));
			} else {
				result.add(AjaxUtils.getAJAXPageDataDefault(httpRequest, GRID_FIELD_NAMES_SEARCH, GRID_FIELD_DEFAULT_VALUES_SEARCH, gridDatas));
			}
			return result;
		} catch (Exception ex) {
			log.error("載入頁面顯示的明細發生錯誤，原因：" + ex.toString());
			ex.printStackTrace();
			throw new Exception("載入頁面顯示的明細失敗！");
		}
	}

	/**
	 * 更新PAGE的LINE
	 *
	 * @param httpRequest
	 * @return List<Properties>
	 * @throws Exception
	 */
	public List<Properties> updateAJAXPageLinesData(Properties httpRequest) throws Exception {
		System.out.println("updateAJAXPageLinesData..");
		String errorMsg = null;

		try {
			String gridData = httpRequest.getProperty(AjaxUtils.GRID_DATA);
			int gridLineFirstIndex = NumberUtils.getInt(httpRequest.getProperty(AjaxUtils.GRID_LINE_FIRST_INDEX));
			int gridRowCount = NumberUtils.getInt(httpRequest.getProperty(AjaxUtils.GRID_ROW_COUNT));
			Long sysSno = NumberUtils.getLong(httpRequest.getProperty("sysSno"));
			String tableName = httpRequest.getProperty("tableName");
			String status = httpRequest.getProperty("status");
			String sysModifierAmail = httpRequest.getProperty("sysModifierAmail");
			// tableName預防輸入小寫
			if (tableName != null) {
				tableName = tableName.toUpperCase();
			}
			log.info("我的status : " + status + ", 我的sysSno : " + sysSno);
			log.info("我的tableName : " + tableName);
			
			if (sysSno == null) {
				throw new ValidationErrorException("傳入的主鍵為空值！");
			}
			
			if (OrderStatus.SAVE.equals(status)) {
				DbcInformationTablesTXF dbcInformationTablesTXF = new DbcInformationTablesTXF();
				dbcInformationTablesTXF.setSysSno(sysSno);
				dbcInformationTablesTXF.setTableName(tableName);
				// 將STRING資料轉成List Properties record data
				List<Properties> upRecords = AjaxUtils.getGridFieldValue(gridData, gridLineFirstIndex, gridRowCount,GRID_FIELD_NAMES);
				// Get INDEX NO 取最後一筆index
				int indexNo = dbcInformationColumnsTXFDAO.findPageLineMaxIndex(sysSno).intValue();
				
				if (upRecords != null && upRecords.size() > 0) {
					for (Properties upRecord : upRecords) {

						String columnName = upRecord.getProperty("columnName");
						log.info("upRecord的columnName:" + columnName);

						if (StringUtils.hasText(tableName) && StringUtils.hasText(columnName)) {
							
							// 前端sysSno、tableName、columnName找身體是否有
							DbcInformationColumnsTXF dbcInformationColumnsTXF = dbcInformationColumnsTXFDAO.findByIdentification(sysSno, tableName, columnName);
							if (dbcInformationColumnsTXF != null) {
								log.info("update欄位明細");
								// columnName預防輸入小寫
								if (columnName != null) {
									columnName = columnName.toUpperCase();
								}
								AjaxUtils.setPojoProperties(dbcInformationColumnsTXF,upRecord, GRID_FIELD_NAMES,GRID_FIELD_TYPES);
								dbcInformationColumnsTXF.setColumnName(columnName);
								dbcInformationColumnsTXF.setSysLastUpdateTime(new Date());
								dbcInformationColumnsTXF.setSysModifierAmail(sysModifierAmail);
								dbcInformationColumnsTXFDAO.update(dbcInformationColumnsTXF);
							} else {
								indexNo++;//最後一筆index+1傳入
								log.info("insert欄位明細");
								// columnName預防輸入小寫
								if (columnName != null) {
									columnName = columnName.toUpperCase();
								}
								DbcInformationColumnsTXF newDbcInformationColumnsTXF = new DbcInformationColumnsTXF();
								AjaxUtils.setPojoProperties(newDbcInformationColumnsTXF,upRecord, GRID_FIELD_NAMES,GRID_FIELD_TYPES);
								newDbcInformationColumnsTXF.setpSysSno(sysSno); //將頭的sysSno傳入身體pSysSno
								newDbcInformationColumnsTXF.setTableName(tableName);
								newDbcInformationColumnsTXF.setColumnName(columnName);
								newDbcInformationColumnsTXF.setColumnIndex(Long.valueOf(indexNo));//最後一筆index+1後傳入
								newDbcInformationColumnsTXF.setSysLastUpdateTime(new Date());
								newDbcInformationColumnsTXF.setSysModifierAmail(sysModifierAmail);
								newDbcInformationColumnsTXF.setDbcInformationTablesTXF(dbcInformationTablesTXF);
								dbcInformationColumnsTXFDAO.save(newDbcInformationColumnsTXF);
							}
						}
					}
				}
			}

		} catch (Exception e) {
			log.error("更新欄位資訊檔明細時發生錯誤，原因：" + e.toString());
			e.printStackTrace();
			throw new Exception("更新欄位資訊檔明細失敗！" + e.getMessage());
		}
		return AjaxUtils.getResponseMsg(errorMsg);

	}

	/**
	 * 更新異動檔及明細檔
	 * 
	 * @param parameterMap
	 * @return Map
	 */
	public Map updateTablesTXFBean(Map parameterMap) throws FormException, Exception {
		log.info("updateTablesTXFBean");
		HashMap resultMap = new HashMap();
		String resultMsg = null;
		try {
			Object formBindBean = parameterMap.get("vatBeanFormBind");
			Object otherBean = parameterMap.get("vatBeanOther");
			String formStatus = (String)PropertyUtils.getProperty(otherBean, "formStatus");
    		String loginEmployeeCode = (String)PropertyUtils.getProperty(otherBean, "loginEmployeeCode");      	
			Long sysSno = NumberUtils.getLong((String) PropertyUtils.getProperty(formBindBean, "sysSno"));
			
			// 取得欲更新的bean
			DbcInformationTablesTXF dbcInformationTablesTXF = this.findById(sysSno);
			if(OrderStatus.SAVE.equals(formStatus)) {
				AjaxUtils.copyJSONBeantoPojoBean(formBindBean, dbcInformationTablesTXF);
			//檢核資料
				//預防輸入小寫
				if(dbcInformationTablesTXF.getTableName() != null) {
					dbcInformationTablesTXF.setTableName(dbcInformationTablesTXF.getTableName().toUpperCase());
				}
			}
			dbcInformationTablesTXF.setStatus(formStatus);
			modifyDbcInformationTablesTXFOrderNo(dbcInformationTablesTXF, loginEmployeeCode);
			parameterMap.put("entityBean", dbcInformationTablesTXF);
			
			resultMap.put("entityBean", dbcInformationTablesTXF);
			resultMsg = "表格名稱 ：" + dbcInformationTablesTXF.getTableName() + "存檔成功！ 是否繼續新增？";
			resultMap.put("resultMsg", resultMsg);
			
			return resultMap;
		} catch (Exception ex) {
			log.error("表格修改單存檔時發生錯誤，原因：" + ex.toString());
			throw new Exception(ex.getMessage());
		}
	}

	/**
	 * update
	 * 
	 * @param updateObj
	 * @return
	 * @throws Exception
	 */
	public String update(DbcInformationTablesTXF updateObj) throws FormException, Exception {
		log.info("DbcInformationTablesTXFService.update ");
		updateObj.setSysLastUpdateTime(new Date());
		dbcInformationTablesTXFDAO.update(updateObj);
		return MessageStatus.SUCCESS;
	}

	/**
     * 暫存單號取實際單號並更新至異動檔及明細檔
     * 
     * @param promotion
     * @param loginUser
     * @return String
     * @throws ObtainSerialNoFailedException
     * @throws FormException
     * @throws Exception
     */
    private String modifyDbcInformationTablesTXFOrderNo(DbcInformationTablesTXF dbcInformationTablesTXF, String loginUser)
    throws ObtainSerialNoFailedException, FormException, Exception {
    	log.info("modifyDbcInformationTablesTXFOrderNo");
    	if (AjaxUtils.isTmpOrderNo(dbcInformationTablesTXF.getOrderNo())) {
    		String serialNo = buOrderTypeService.getOrderSerialNo(
    				"T2", dbcInformationTablesTXF.getOrderTypeCode());
    		if (!serialNo.equals("unknow")) {
    			dbcInformationTablesTXF.setOrderNo(serialNo);
    		} else {
    			throw new ObtainSerialNoFailedException("取得" + dbcInformationTablesTXF.getOrderTypeCode() + "單號失敗！");
    		}
    	}
    	dbcInformationTablesTXF.setSysModifierAmail(loginUser);
		dbcInformationTablesTXF.setSysLastUpdateTime(new Date());
		dbcInformationTablesTXFDAO.update(dbcInformationTablesTXF);
    	return dbcInformationTablesTXF.getOrderTypeCode() + "-" + dbcInformationTablesTXF.getOrderNo() + "存檔成功！";
    }
	/**
	 * 暫存單號取實際單號並更新至表格欄位修改單
	 * 
	 * @param sysSno
	 * @param loginUser
	 * @return String
	 * @throws ObtainSerialNoFailedException
	 * @throws FormException
	 * @throws Exception
	 */
	public DbcInformationTablesTXF saveActualOrderNo(Long sysSno, String loginUser)
			throws ObtainSerialNoFailedException, FormException, Exception {
		log.info("saveActualOrderNo");		
		DbcInformationTablesTXF dbcInformationTablesTXF = this.findById(sysSno);
		if(dbcInformationTablesTXF == null) {
			throw new NoSuchObjectException("查無單主鍵(" + sysSno + ")的資料！");
		}else if(AjaxUtils.isTmpOrderNo(dbcInformationTablesTXF.getOrderNo())) {
			String serialNo = buOrderTypeService.getOrderSerialNo("T2", dbcInformationTablesTXF.getOrderTypeCode());
			if (!serialNo.equals("unknow")) {
				dbcInformationTablesTXF.setOrderNo(serialNo);
			} else {
				throw new ObtainSerialNoFailedException("取得" + dbcInformationTablesTXF.getOrderTypeCode() + "單號失敗！");
			}
		}
		dbcInformationTablesTXF.setSysModifierAmail(loginUser);
		dbcInformationTablesTXF.setSysLastUpdateTime(new Date());
		dbcInformationTablesTXFDAO.update(dbcInformationTablesTXF);
		
		return dbcInformationTablesTXF;
	}
	
	/**
	 * 完成將異動檔更新至主檔
	 * @param tableName
	 * @param loginEmployeeCode
	 */
	public void saveFinishDbcInformation(Long sysSno, String loginEmployeeCode)throws IndexOutOfBoundsException {
		log.info("saveFinishDbcInformation");
		// 找異動檔頭dbcInformationTablesTXF,將狀態改為FINISH
		DbcInformationTablesTXF dbcInformationTablesTXF = this.findById(sysSno);
		dbcInformationTablesTXF.setStatus(OrderStatus.FINISH);
		
		// 找主檔頭dbcInformationTablesList
		List<DbcInformationTables> dbcInformationTablesList = dbcInformationTablesDAO.findByProperty("DbcInformationTables",
				new String[] {"area","tableName","enable"},
				new String[] { dbcInformationTablesTXF.getArea(),dbcInformationTablesTXF.getTableName(),
							   dbcInformationTablesTXF.getEnable() });
		log.info("dbcInformationTablesList.size = " + dbcInformationTablesList.size());
		
		// 取異動檔身dbcInformationColumnsTXFList
		List<DbcInformationColumnsTXF> dbcInformationColumnsTXFList = dbcInformationTablesTXF.getDbcInformationColumnsTXFList();
		log.info("dbcInformationColumnsTXFList.size = " + dbcInformationColumnsTXFList.size());
		
		DbcInformationTables dbcInformationTables = null; //宣告主檔頭
		DbcInformationColumns dbcInformationColumns = null; //宣告主檔身
		
		//DB若沒有主檔頭,則"新增"
		if (dbcInformationTablesList == null || dbcInformationTablesList.size() == 0) {
			log.info("INSERT");
			/** INSERT主檔頭 */
			dbcInformationTables = new DbcInformationTables();
			dbcInformationTables.setArea(dbcInformationTablesTXF.getArea());
			dbcInformationTables.setTableName(dbcInformationTablesTXF.getTableName());
			dbcInformationTables.setTableComments(dbcInformationTablesTXF.getTableComments());
			dbcInformationTables.setEnable(dbcInformationTablesTXF.getEnable());
			dbcInformationTables.setSysModifierAmail(loginEmployeeCode);
			dbcInformationTables.setSysLastUpdateTime(new Date());
			dbcInformationTables.setDbcInformationColumnsList(new ArrayList<DbcInformationColumns>());
			/** INSERT主檔身(異動身迴圈) */
			for(DbcInformationColumnsTXF dbcInformationColumnsTXF : dbcInformationColumnsTXFList ) {
				dbcInformationColumns = new DbcInformationColumns();
				dbcInformationColumns.setTableName(dbcInformationColumnsTXF.getTableName());
				dbcInformationColumns.setColumnName(dbcInformationColumnsTXF.getColumnName());
				dbcInformationColumns.setColumnComments(dbcInformationColumnsTXF.getColumnComments());
				dbcInformationColumns.setColumnIndex(dbcInformationColumnsTXF.getColumnIndex());
				dbcInformationColumns.setDataType(dbcInformationColumnsTXF.getDataType());
				dbcInformationColumns.setDataSize(dbcInformationColumnsTXF.getDataSize());
				dbcInformationColumns.setEnable(dbcInformationColumnsTXF.getEnable());
				dbcInformationColumns.setIsRowId(dbcInformationColumnsTXF.getIsRowId());
				dbcInformationColumns.setNotNull(dbcInformationColumnsTXF.getNotNull());
				dbcInformationColumns.setSysModifierAmail(loginEmployeeCode);
				dbcInformationColumns.setSysLastUpdateTime(new Date());
				dbcInformationTables.getDbcInformationColumnsList().add(dbcInformationColumns);
			}
		} 
		//DB若有主檔頭,則"修改"
		else {
			log.info("UPDATE");
			/** UPDATE主檔頭 */
			dbcInformationTables = dbcInformationTablesList.get(0);
			dbcInformationTables.setTableComments(dbcInformationTablesTXF.getTableComments());
			dbcInformationTables.setEnable(dbcInformationTablesTXF.getEnable());
			dbcInformationTables.setSysModifierAmail(loginEmployeeCode);
			dbcInformationTables.setSysLastUpdateTime(new Date());
			
			// 取主檔身dbcInformationColumnsList
			List<DbcInformationColumns> dbcInformationColumnsList = dbcInformationTables.getDbcInformationColumnsList();
			log.info("dbcInformationColumnsList.size = " + dbcInformationColumnsList.size());
			
			// Get INDEX NO
			int count = 0;
			int countTXF = 0;
			int maxIndex = dbcInformationColumnsList.size()-1;//主檔身最後的index
			/** UPDATE主檔身(異動身迴圈) */
			for(countTXF = 0; countTXF < dbcInformationColumnsTXFList.size(); ) {
				DbcInformationColumnsTXF dbcInformationColumnsTXF = dbcInformationColumnsTXFList.get(countTXF); //每一個異動身
				if(count>maxIndex) { //主檔count大於最後一筆index
					dbcInformationColumns = dbcInformationColumnsList.get(maxIndex); //取得最後一筆主檔身
				}else {
					dbcInformationColumns = dbcInformationColumnsList.get(count); //每一個主檔身
				}
				//比對每一個主檔身與異動身的columnIndex
				if(dbcInformationColumns.getColumnIndex().equals(dbcInformationColumnsTXF.getColumnIndex())) {
					log.info("update:第"+dbcInformationColumnsTXF.getColumnIndex()+"筆明細");
					dbcInformationColumns.setColumnComments(dbcInformationColumnsTXF.getColumnComments());
					dbcInformationColumns.setColumnIndex(dbcInformationColumnsTXF.getColumnIndex());
					dbcInformationColumns.setDataType(dbcInformationColumnsTXF.getDataType());
					dbcInformationColumns.setDataSize(dbcInformationColumnsTXF.getDataSize());
					dbcInformationColumns.setEnable(dbcInformationColumnsTXF.getEnable());
					dbcInformationColumns.setIsRowId(dbcInformationColumnsTXF.getIsRowId());
					dbcInformationColumns.setNotNull(dbcInformationColumnsTXF.getNotNull());
					dbcInformationColumns.setSysModifierAmail(loginEmployeeCode);
					dbcInformationColumns.setSysLastUpdateTime(new Date());
					countTXF++;
					count++;
				}else if(count <= dbcInformationColumnsList.size()){	
					count++;
				}else {
					dbcInformationColumns = new DbcInformationColumns();
					log.info("insert:第"+dbcInformationColumnsTXF.getColumnIndex()+"筆明細");
					dbcInformationColumns.setTableName(dbcInformationColumnsTXF.getTableName());
					dbcInformationColumns.setColumnName(dbcInformationColumnsTXF.getColumnName());
					dbcInformationColumns.setColumnComments(dbcInformationColumnsTXF.getColumnComments());
					dbcInformationColumns.setColumnIndex(dbcInformationColumnsTXF.getColumnIndex());
					dbcInformationColumns.setDataType(dbcInformationColumnsTXF.getDataType());
					dbcInformationColumns.setDataSize(dbcInformationColumnsTXF.getDataSize());
					dbcInformationColumns.setEnable(dbcInformationColumnsTXF.getEnable());
					dbcInformationColumns.setIsRowId(dbcInformationColumnsTXF.getIsRowId());
					dbcInformationColumns.setNotNull(dbcInformationColumnsTXF.getNotNull());
					dbcInformationColumns.setSysModifierAmail(loginEmployeeCode);
					dbcInformationColumns.setSysLastUpdateTime(new Date());
					dbcInformationTables.getDbcInformationColumnsList().add(dbcInformationColumns);
					countTXF++;
				}
			}
			dbcInformationTables.setDbcInformationColumnsList(dbcInformationColumnsList);
		}
		dbcInformationTablesDAO.saveOrUpdate(dbcInformationTables);
		dbcInformationTablesTXFDAO.update(dbcInformationTablesTXF);
	}
	
	
	
	
	
	
	
}

