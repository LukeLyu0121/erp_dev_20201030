package tw.com.tm.erp.hbm;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class SpringUtils {

	private static ApplicationContext context;
	private static String springConfigFile = "spring-config.xml" ;
	private static String lastSpringConfigFile = "spring-config.xml" ;

	public static ApplicationContext getApplicationContext() {
		//System.out.println("Set config file ->" + springConfigFile + " , old ->" + lastSpringConfigFile);
		if ((context == null)||(!springConfigFile.equalsIgnoreCase(lastSpringConfigFile))) {
			System.out.println("Set config file ->" + springConfigFile);
			context = null ;
			System.gc() ;
			context = new ClassPathXmlApplicationContext(springConfigFile);
			lastSpringConfigFile = springConfigFile ;
		}
		return context;
	}

	public static String getSpringConfigFile() {
		return springConfigFile;
	}

	public static void setSpringConfigFile(String springConfigFile) {
		SpringUtils.springConfigFile = springConfigFile;
	}
	 
}
