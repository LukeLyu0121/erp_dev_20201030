package tw.com.tm.erp.hbm.dao;

<<<<<<< HEAD
import java.util.List;

import tw.com.tm.erp.hbm.bean.DbcInformationTables;

public class DbcInformationTablesDAO extends BaseDAO{
	
	public DbcInformationTables findDbcInformationTablesByIdentification(String tableName) {
		
		StringBuffer hql = new StringBuffer("from DbcInformationTables as model where 1 = 1");
		hql.append(" and model.tableName = ?");
		List<DbcInformationTables> result = getHibernateTemplate().find(hql.toString(),
				new Object[] { tableName});

		return (result != null && result.size() > 0 ? result.get(0) : null);
		
	}

=======
import java.sql.SQLException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;
import org.springframework.orm.hibernate3.HibernateCallback;
import org.springframework.util.StringUtils;

import tw.com.tm.erp.hbm.bean.DbcInformationTables;

public class DbcInformationTablesDAO extends BaseDAO {
	private static final Log log = LogFactory.getLog(DbcInformationTablesDAO.class);

	
	public DbcInformationTables findByIdentification(String tableName) {

		StringBuffer hql = new StringBuffer("from DbcInformationTables as model where 1 = 1");
		hql.append(" and model.tableName = ?");
		List<DbcInformationTables> result = getHibernateTemplate().find(hql.toString(), new Object[] { tableName });
		return (result != null && result.size() > 0 ? result.get(0) : null);
	}
	
	public List<DbcInformationTables> findByArea(String area) {
		try {

		    StringBuffer hql = new StringBuffer("from DbcInformationTables as model ");
		    hql.append("where model.id.area = ? ");
		    hql.append("and model.enable = ? ");
		    hql.append("order by tableName");
		    
		    return getHibernateTemplate().find(hql.toString(),
			    new Object[] { area, "Y" });
		} catch (RuntimeException re) {
			throw re;
		}
	}
	
	
	
>>>>>>> brian_dev
}
