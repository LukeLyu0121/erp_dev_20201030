package tw.com.tm.erp.hbm.dao;

import java.util.List;
import java.sql.SQLException;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.orm.hibernate3.HibernateCallback;
import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;
import tw.com.tm.erp.hbm.bean.DbcInformationColumnsTXF;
import tw.com.tm.erp.hbm.bean.DbcInformationTablesTXF;

public class DbcInformationColumnsTXFDAO extends BaseDAO {
	private static final Log log = LogFactory.getLog(DbcInformationTablesTXFDAO.class);

	/**
	 * find page line
	 * 
	 * @param sysSno
	 * @param startPage
	 * @param pageSize
	 * @return List<DbcInformationColumnsTXF>
	 */
	public List<DbcInformationColumnsTXF> findPageColumns(Long sysSno, int startPage, int pageSize) {
		final int startRecordIndexStar = startPage * pageSize;
		final int pSize = pageSize;
		final Long sNo = sysSno;
		List<DbcInformationColumnsTXF> result = getHibernateTemplate().executeFind(new HibernateCallback() {
			public Object doInHibernate(Session session) throws HibernateException, SQLException {
				StringBuffer hql = new StringBuffer("from DbcInformationColumnsTXF as model where 1=1 ");
				if (sNo != null)
					hql.append(" and model.DbcInformationColumnsTXF.sysSno = :sysSno order by sysSno");
				Query query = session.createQuery(hql.toString());
				query.setFirstResult(startRecordIndexStar);
				query.setMaxResults(pSize);
				if (sNo != null)
					query.setLong("tableName", sNo);
				return query.list();
			}
		});
		return result;
	}

	/**
	 * find page line 最後一筆 index
	 * 
	 * @param sysSno
	 * @return Long
	 */
	public Long findPageLineMaxIndex(Long sysSno) {

		Long lineMaxIndex = new Long(0);
		final Long sSno = sysSno;
		List<DbcInformationTablesTXF> result = getHibernateTemplate().executeFind(new HibernateCallback() {
			public Object doInHibernate(Session session) throws HibernateException, SQLException {
				StringBuffer hql = new StringBuffer("from DbcInformationTablesTXF as model where 1=1 ");
				if (sSno != null)
					hql.append(" and model.sysSno = :sysSno");
				Query query = session.createQuery(hql.toString());
				if (sSno != null)
					query.setLong("sysSno", sSno);
				return query.list();
			}
		});
		if (result != null && result.size() > 0) {
			List<DbcInformationColumnsTXF> dbcInformationColumnsTXF = result.get(0).getDbcInformationColumnsTXFList();
			if (dbcInformationColumnsTXF != null && dbcInformationColumnsTXF.size() > 0) {
				lineMaxIndex = dbcInformationColumnsTXF.get(dbcInformationColumnsTXF.size() - 1).getColumnIndex();
			}
		}
		return lineMaxIndex;
	}

	/**
	 * find line用pSysSno & tableName & columnName
	 * 
	 * @param pSysSno
	 * @param tableName
	 * @param columnName
	 * @return DbcInformationColumnsTXF
	 */
	public DbcInformationColumnsTXF findByIdentification(Long pSysSno, String tableName, String columnName) {
		StringBuffer hql = new StringBuffer(
				"from DbcInformationColumnsTXF as model where model.id.pSysSno = ? and model.id.tableName = ? and model.id.columnName = ?");
		List<DbcInformationColumnsTXF> result = getHibernateTemplate().find(hql.toString(),
				new Object[] { pSysSno, tableName, columnName });
		return (result != null && result.size() > 0 ? result.get(0) : null);
	}

}
