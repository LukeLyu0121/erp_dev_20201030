package tw.com.tm.erp.hbm.dao;


import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import tw.com.tm.erp.hbm.bean.DbcInformationColumnsTXF;
import tw.com.tm.erp.hbm.bean.DbcInformationTables;
import tw.com.tm.erp.hbm.bean.DbcInformationTablesTXF;

public class DbcInformationTablesTXFDAO extends BaseDAO {

	private static final Log log = LogFactory.getLog(DbcInformationTablesTXFDAO.class);

	public DbcInformationTablesTXF findById(Long sysSno) {
		return (DbcInformationTablesTXF) findByPrimaryKey(DbcInformationTablesTXF.class, sysSno);
	}
	
	public DbcInformationTablesTXF findByIdentification(String tableName) {

		StringBuffer hql = new StringBuffer("from DbcInformationTablesTXF as model where 1 = 1");
		hql.append(" and model.tableName = ?");
		List<DbcInformationTablesTXF> result = getHibernateTemplate().find(hql.toString(), new Object[] { tableName });
		return (result != null && result.size() > 0 ? result.get(0) : null);

	}
	
	public DbcInformationTablesTXF findByIdentification(String area, String tableName) {

		StringBuffer hql = new StringBuffer("from DbcInformationTablesTXF as model where 1 = 1");
		hql.append(" and model.area = ?");
		hql.append(" and model.tableName = ?");
		List<DbcInformationTablesTXF> result = getHibernateTemplate().find(hql.toString(), new Object[] { area, tableName});
		return (result != null && result.size() > 0 ? result.get(0) : null);
	}

}
