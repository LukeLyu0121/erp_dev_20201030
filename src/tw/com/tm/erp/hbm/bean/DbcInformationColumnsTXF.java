package tw.com.tm.erp.hbm.bean;

import java.util.Date;

public class DbcInformationColumnsTXF implements java.io.Serializable{


    /**
	 * 
	 */
	private static final long serialVersionUID = -6284486417225917235L;
	// Fields    

     private Long sysSno;
     private Long pSysSno;
     private String tableName;
     private Long columnIndex;
     private String columnName;
     private String columnComments;
     private String dataType;
     private Long dataSize;
     private String isRowId;
     private String notNull;
     private String enable;
     private String sysSign;
     private String sysVerifyCode;
     private String sysModifierAmail;
     private Date sysLastUpdateTime;
     private DbcInformationTablesTXF dbcInformationTablesTXF;

    // Constructors

    public DbcInformationTablesTXF getDbcInformationTablesTXF() {
		return dbcInformationTablesTXF;
	}


	public void setDbcInformationTablesTXF(DbcInformationTablesTXF dbcInformationTablesTXF) {
		this.dbcInformationTablesTXF = dbcInformationTablesTXF;
	}


	/** default constructor */
    public DbcInformationColumnsTXF() {
    }

    
    /** full constructor */
    public DbcInformationColumnsTXF(Long sysSno, Long pSysSno, String tableName, Long columnIndex, String columnName,
			String columnComments, String dataType, Long dataSize, String isRowId, String notNull, String enable,
			String sysSign, String sysVerifyCode, String sysModifierAmail, Date sysLastUpdateTime,
			DbcInformationTablesTXF dbcInformationTablesTXF) {
		super();
		this.sysSno = sysSno;
		this.pSysSno = pSysSno;
		this.tableName = tableName;
		this.columnIndex = columnIndex;
		this.columnName = columnName;
		this.columnComments = columnComments;
		this.dataType = dataType;
		this.dataSize = dataSize;
		this.isRowId = isRowId;
		this.notNull = notNull;
		this.enable = enable;
		this.sysSign = sysSign;
		this.sysVerifyCode = sysVerifyCode;
		this.sysModifierAmail = sysModifierAmail;
		this.sysLastUpdateTime = sysLastUpdateTime;
		this.dbcInformationTablesTXF = dbcInformationTablesTXF;
	}
    
    // Property accessors
    
	public Long getSysSno() {
		return sysSno;
	}

	public void setSysSno(Long sysSno) {
		this.sysSno = sysSno;
	}
	
	public Long getpSysSno() {
		return pSysSno;
	}

	public void setpSysSno(Long pSysSno) {
		this.pSysSno = pSysSno;
	}

	public String getTableName() {
		return tableName;
	}


	public void setTableName(String tableName) {
		this.tableName = tableName;
	}


	public Long getColumnIndex() {
		return columnIndex;
	}


	public void setColumnIndex(Long columnIndex) {
		this.columnIndex = columnIndex;
	}


	public String getColumnName() {
		return columnName;
	}


	public void setColumnName(String columnName) {
		this.columnName = columnName;
	}


	public String getColumnComments() {
		return columnComments;
	}


	public void setColumnComments(String columnComments) {
		this.columnComments = columnComments;
	}


	public String getDataType() {
		return dataType;
	}


	public void setDataType(String dataType) {
		this.dataType = dataType;
	}


	public Long getDataSize() {
		return dataSize;
	}


	public void setDataSize(Long dataSize) {
		this.dataSize = dataSize;
	}


	public String getIsRowId() {
		return isRowId;
	}


	public void setIsRowId(String isRowId) {
		this.isRowId = isRowId;
	}


	public String getNotNull() {
		return notNull;
	}


	public void setNotNull(String notNull) {
		this.notNull = notNull;
	}


	public String getEnable() {
		return enable;
	}


	public void setEnable(String enable) {
		this.enable = enable;
	}


	public String getSysSign() {
		return sysSign;
	}


	public void setSysSign(String sysSign) {
		this.sysSign = sysSign;
	}


	public String getSysVerifyCode() {
		return sysVerifyCode;
	}


	public void setSysVerifyCode(String sysVerifyCode) {
		this.sysVerifyCode = sysVerifyCode;
	}


	public String getSysModifierAmail() {
		return sysModifierAmail;
	}


	public void setSysModifierAmail(String sysModifierAmail) {
		this.sysModifierAmail = sysModifierAmail;
	}


	public Date getSysLastUpdateTime() {
		return sysLastUpdateTime;
	}


	public void setSysLastUpdateTime(Date sysLastUpdateTime) {
		this.sysLastUpdateTime = sysLastUpdateTime;
	}


	public static long getSerialversionuid() {
		return serialVersionUID;
	}

   
   

    
   








}
