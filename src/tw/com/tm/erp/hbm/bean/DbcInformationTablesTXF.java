package tw.com.tm.erp.hbm.bean;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import tw.com.tm.erp.utils.DateUtils;

public class DbcInformationTablesTXF implements java.io.Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -7384430967407090785L;
	// Fields

	private Long sysSno;
	private String area;
	private String tableName;
	private String tableComments;
	private String enable;
	private String sysSign;
	private String sysVerifyCode;
	private String sysModifierAmail;
	private Date sysLastUpdateTime;
	private String orderTypeCode;
	private String orderNo;
	private String status;
	private List<DbcInformationColumnsTXF> dbcInformationColumnsTXFList;
	/**
	 * 假bean
	 */
	private String sysLastUpdateTimeStr;

	public String getSysLastUpdateTimeStr() {
		return sysLastUpdateTimeStr;
	}

	public void setSysLastUpdateTimeStr(String sysLastUpdateTimeStr) {
		this.sysLastUpdateTimeStr = sysLastUpdateTimeStr;
	}

	// Constructors

	public List<DbcInformationColumnsTXF> getDbcInformationColumnsTXFList() {
		return dbcInformationColumnsTXFList;
	}

	public void setDbcInformationColumnsTXFList(List<DbcInformationColumnsTXF> dbcInformationColumnsTXFList) {
		this.dbcInformationColumnsTXFList = dbcInformationColumnsTXFList;
	}

	/** default constructor */
	public DbcInformationTablesTXF() {
	}

	/** full constructor */
	public DbcInformationTablesTXF(Long sysSno, String area, String tableName, String tableComments, String enable,
			String sysSign, String sysVerifyCode, String sysModifierAmail, Date sysLastUpdateTime, String orderTypeCode,
			String orderNo, String status, List<DbcInformationColumnsTXF> dbcInformationColumnsTXFList) {
		super();
		this.sysSno = sysSno;
		this.area = area;
		this.tableName = tableName;
		this.tableComments = tableComments;
		this.enable = enable;
		this.sysSign = sysSign;
		this.sysVerifyCode = sysVerifyCode;
		this.sysModifierAmail = sysModifierAmail;
		this.sysLastUpdateTime = sysLastUpdateTime;
		this.orderTypeCode = orderTypeCode;
		this.orderNo = orderNo;
		this.status = status;
		this.dbcInformationColumnsTXFList = dbcInformationColumnsTXFList;
	}

	// Property accessors

	public Long getSysSno() {
		return this.sysSno;
	}

	public void setSysSno(Long sysSno) {
		this.sysSno = sysSno;
	}

	public String getArea() {
		return this.area;
	}

	public void setArea(String area) {
		this.area = area;
	}

	public String getTableName() {
		return this.tableName;
	}

	public void setTableName(String tableName) {
		this.tableName = tableName;
	}

	public String getTableComments() {
		return this.tableComments;
	}

	public void setTableComments(String tableComments) {
		this.tableComments = tableComments;
	}

	public String getEnable() {
		return this.enable;
	}

	public void setEnable(String enable) {
		this.enable = enable;
	}

	public String getSysSign() {
		return sysSign;
	}

	public void setSysSign(String sysSign) {
		this.sysSign = sysSign;
	}

	public String getSysVerifyCode() {
		return sysVerifyCode;
	}

	public void setSysVerifyCode(String sysVerifyCode) {
		this.sysVerifyCode = sysVerifyCode;
	}

	public String getSysModifierAmail() {
		return sysModifierAmail;
	}

	public void setSysModifierAmail(String sysModifierAmail) {
		this.sysModifierAmail = sysModifierAmail;
	}

	public Date getSysLastUpdateTime() {
		return sysLastUpdateTime;
	}

	public void setSysLastUpdateTime(Date sysLastUpdateTime) {
		this.sysLastUpdateTime = sysLastUpdateTime;
		if(sysLastUpdateTime != null) {
			this.setSysLastUpdateTimeStr(DateUtils.formatTime(sysLastUpdateTime));
		}
	}

	public String getOrderTypeCode() {
		return this.orderTypeCode;
	}

	public void setOrderTypeCode(String orderTypeCode) {
		this.orderTypeCode = orderTypeCode;
	}

	public String getOrderNo() {
		return this.orderNo;
	}

	public void setOrderNo(String orderNo) {
		this.orderNo = orderNo;
	}

	public String getStatus() {
		return this.status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

}
