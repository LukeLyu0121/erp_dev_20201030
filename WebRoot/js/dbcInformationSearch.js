/**
 *	檔案: dbcInformationSearch.js
 *	說明: 表格欄位資訊查詢作業
 *  Brian
 */
 
 // 設定全域變數
var vnB_Detail = 2;
var vnB_Header = 1;
// 程式進入點
function kweImBlock(){
  searchInitial();
  headerInitial();
  buttonLine();
  detailInitial();
}

// 搜尋初始化
function searchInitial(){ 
     vat.bean().vatBeanOther = 
  	    {
  	     loginBrandCode     : "T2",   	
  	     loginEmployeeCode  : "Brian"	
	    };
}

// 可搜尋的欄位
function headerInitial(){
	var enableStatus = [["", false, false], ["啟用","停用"], ["Y", "N"]];
	var allArea = [["", true, false], ["請選擇","GLOBAL","LOCAL"], ["","GLOBAL","LOCAL"]];
	vat.block.create( vnB_Header, {
		id: "vatBlock_Head", generate: true,
		table:"cellspacing='1' class='default' border='0' cellpadding='2'",
		title:"表格欄位資訊查詢作業", rows:[  
	 		{row_style:"", cols:[
	 			{items:[{name:"#L.area",		type:"LABEL", 	value:"區域"}]},
				{items:[{name:"#F.area",		type:"SELECT", init:allArea, size:20 /*, eChange:"changeArea()"*/}]},
				{items:[{name:"#L.tableName",	type:"LABEL", value:"schema名稱"}]},
				{items:[{name:"#F.schema",   	type:"TEXT", bind:"schema" ,size:20}]},
//				{items:[{name:"#L.tableName",	type:"LABEL", value:"表格名稱"}]},
//				{items:[{name:"#F.tableName",   type:"SELECT", bind:"tableName" ,size:20}]},
				{items:[{name:"#L.enable",		type:"LABEL", value:"啟用狀態"}]},
				{items:[{name:"#F.enable",   	type:"SELECT",init:enableStatus,  bind:"enable", size:20}]}
			]}
			
		], 	 
		beginService:"",
		closeService:""			
	});

}

function buttonLine(){
    vat.block.create(vnB_Button = 0, {
	id: "vatBlock_Button", generate: true,
	table:"cellspacing='0' class='default' border='0' cellpadding='0' style='border-width: 5px; border-left-width: 5px; border-color: #ffffff;'",	
	title:"", rows:[  
	 {row_style:"", cols:[
	 	{items:[{name:"#B.search"      , type:"IMG"      , value:"查詢",  src:"./images/button_find.gif", eClick:"doSearch()"},
	 			{name:"SPACE"          , type:"LABEL"    , value:"　"},
	 			{name:"#B.clear"       , type:"IMG"      , value:"清除",  src:"./images/button_reset.gif"  , eClick:""},
	 			{name:"SPACE"          , type:"LABEL"    , value:"　"},
	 	 		{name:"#B.exit"        , type:"IMG"      , value:"離開",   src:"./images/button_exit.gif"  , eClick:'closeWindows("CONFIRM")'},
	 	 		{name:"SPACE"          , type:"LABEL"    , value:"　"},
	 			{name:"#B.update"      , type:"IMG"      , value:"檢視",   src:"./images/button_view.gif"  , eClick:"doClosePicker()"}],
	 			  td:"style='background-color:#eeeeee; border-color:#eeeeee; border-width: 8px;'"}]}
	  ], 	 
		beginService:"",
		closeService:""			
	});
}

function detailInitial(){
  
  var vbCanGridDelete = false;
  var vbCanGridAppend = false;
  var vbCanGridModify = true;
  var vatPickerId =vat.bean().vatBeanOther.vatPickerId; 
  var vbSelectionType = "CHECK";
     if(vatPickerId != null && vatPickerId != ""){
    	 vat.item.make(vnB_Detail, "checkbox", {type:"XBOX"});
    	 vbSelectionType = "CHECK";    
     }else{
     	 vbSelectionType = "NONE";
     }
    vat.item.make(vnB_Detail, "indexNo"         , {type:"IDX"  , desc:"項次" });
    vat.item.make(vnB_Detail, "area"			, {type:"TEXT" , size:20, maxLen:20, mode:"HIDDEN", desc:"區域"});
	vat.item.make(vnB_Detail, "tableName"		, {type:"TEXT" , size:20, maxLen:20, mode:"READONLY", desc:"表格名稱"});
	vat.item.make(vnB_Detail, "tableComments" 	, {type:"TEXT" , size:20, maxLen:20, mode:"READONLY", desc:"表格說明"});
	vat.item.make(vnB_Detail, "enable"	        , {type:"TEXT" , size:8, maxLen:20, mode:"READONLY", desc:"啟用狀態"});
	vat.item.make(vnB_Detail, "sysSno",          {type:"ROWID"});
	vat.block.pageLayout(vnB_Detail, {
														id                  : "vatDetailDiv",
														pageSize            : 10,
														searchKey           : ["sysSno"],
														selectionType       : vbSelectionType,
														indexType           : "AUTO",
								                        canGridDelete       : vbCanGridDelete,
														canGridAppend       : vbCanGridAppend,
														canGridModify       : vbCanGridModify,	
														loadBeforeAjxService: "loadBeforeAjxService()",	
														loadSuccessAfter    : "loadSuccessAfter()", 						
														saveBeforeAjxService: "saveBeforeAjxService()",
														saveSuccessAfter    : "saveSuccessAfter()",
														indicate            : function(){if(vatPickerId != null && vatPickerId != ""){return false}else{ openModifyPage()} }
														});

}

// 載入成功後
function loadSuccessAfter(){
//	alert("loadSuccessAfter");
	if( vat.block.getGridObject(vnB_Detail).dataCount == vat.block.getGridObject(vnB_Detail).pageSize &&
	    vat.block.getGridObject(vnB_Detail).lastIndex == 1){
		alert("您輸入條件查無資料");
	}else{
//		doFormAccessControl();
	}
}

// 查詢點下執行
function loadBeforeAjxService(){
//   alert("loadBeforeAjxService");	
   
	var processString = "process_object_name=dbcInformationService&process_object_method_name=getAJAXSearchPageData" 
	 				  +"&area="+ vat.item.getValueByName("#F.area") 
					  +"&tableName="+ vat.item.getValueByName("#F.tableName")
					  +"&enable="+ vat.item.getValueByName("#F.enable")
                      +"&schema="+ vat.item.getValueByName("#F.schema")                                                
	return processString;											
}

function saveSuccessAfter(){
}

//	判斷是否要關閉LINE
function checkEnableLine() {
	return true;
}

//	取得SAVE要執行的將搜尋結果存到暫存檔
function saveBeforeAjxService() {
//	alert("saveBeforeAjxService");
	var processString = "";
	if (checkEnableLine()) {
		processString = "process_object_name=buShopMainService&process_object_method_name=saveSearchResult";
	}
	return processString;
}								

// 檢視按下後的動作 
function doClosePicker(){
	   vat.block.pageSearch(vnB_Detail, {
    		funcSuccess : 
    		    function(){
    		      vat.block.submit(
    		                     function(){ return "process_object_name=buShopMainService&process_object_method_name=getSearchSelection";
    		                               }, { bind:true, link:false, other:false, picker:true, isPicker:true, blockId:vnB_Detail} );
    			}}); 
}

// 查詢按下後
function doSearch(){
    vat.block.submit(function(){return "process_object_name=tmpAjaxSearchDataService"+
			                    "&process_object_method_name=deleteByTimeScope&timeScope="+ vat.bean().vatBeanOther.timeScope }, 
			                    {other: true, 
			                     funcSuccess: function() {vat.block.pageDataLoad(vnB_Detail , vnCurrentPage = 1);}
			                    });
}


function closeWindows(closeType){
  var isExit = true ;
  if("CONFIRM" == closeType){
	isExit = confirm("是否確認離開?");
  }
  if(isExit)
    	window.top.close();
    	
}

//function changeArea(){
//    if(vat.item.getValueByName("#F.area") !== ""){    
//       vat.ajax.XHRequest(
//       {
//           post:"process_object_name=dbcInformationService"+
//                    "&process_object_method_name=getTableNameAJAX"+
//                    "&area=" + vat.item.getValueByName("#F.area"),
//           find: function changeShopCodeRequestSuccess(oXHR){ 
//				vat.item.SelectBind(eval(vat.ajax.getValue("allTableName", oXHR.responseText)),{ itemName : "#F.tableName" });
//				
//           }   
//       });
//    }
//}

function openModifyPage(){
    var nItemLine = vat.item.getGridLine();
    var tableName = vat.item.getGridValueByName("tableName", nItemLine);
    var area = vat.item.getGridValueByName("area", nItemLine);
    
    if(null != tableName && "" != tableName && 0 != tableName){
    	var url = "/erp_dev20201030/tm.jsp?vJsName=dbcInformationTXF&vTableName=" + tableName + "&vArea=" + area;	
		sc=window.open(url, '表格欄位資訊維護作業', 'menubar=no,resizable=no,scrollbars=no,status=no,resizable=yes,scrollbars=yes');
		sc.resizeTo((screen.availWidth),(screen.availHeight));
		sc.moveTo(0,0);
	}
}
